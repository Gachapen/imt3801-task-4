#include <cstdio>
#include <thread>
#include <random>
#include <array>
#include <future>
#include <condition_variable>
#include "ThreadSafeQueue.h"

std::condition_variable numberAdditionCondition;
std::mutex numberAdditionMutex;

int additionWorker(ThreadSafeQueue<int>& numberQueue, const bool& finishedProducing)
{
	int sum = 0;
	bool quit = false;

	while (quit == false) {
		std::unique_lock<std::mutex> lock(numberAdditionMutex);

		if (finishedProducing == true) {
			quit = true;
		} else {
			numberAdditionCondition.wait(
				lock,
				[&numberQueue, &finishedProducing] {
					return (numberQueue.isEmpty() == false || finishedProducing == true);
				}
			);
		}

		lock.unlock();

		while (numberQueue.isEmpty() == false) {
			int number = numberQueue.pop();
			sum += number;
		}
	}

	return sum;
}

int main(int argc, char* argv[])
{
	std::random_device randomDevice;
	std::default_random_engine randomEngine(randomDevice());
	std::uniform_int_distribution<int> distribution(0, 99);

	std::array<int, 100> numberList;
	
	// Create random number list.
	for (size_t i = 0; i < numberList.size(); i++) {
		numberList[i] = distribution(randomEngine);
		printf("Random number created: %i\n", numberList[i]);
	}
	
	// Add number list in serial.
	int serialTotal = 0;
	for (size_t i = 0; i < numberList.size(); i++) {
		serialTotal += numberList[i];
	}
	printf("Sum after adding in serial: %i\n", serialTotal);

	ThreadSafeQueue<int> numberQueue;
	std::array<std::future<int>, 8> consumerFutureResults;
	bool finishedProducing = false;

	// Create consumers.
	for (size_t i = 0; i < consumerFutureResults.size(); i++) {
		consumerFutureResults[i] = std::async(std::launch::async, &additionWorker, std::ref(numberQueue), std::ref(finishedProducing));
	}
	
	// Produce numbers for consumers.
	for (size_t i = 0; i < numberList.size(); i++) {
		numberQueue.push(numberList[i]);
		numberAdditionCondition.notify_one();
	}

	// Notify that we've finished producing.
	numberAdditionMutex.lock();
	finishedProducing = true;
	numberAdditionMutex.unlock();
	numberAdditionCondition.notify_all();

	// Sum consumers
	int parallelTotal = 0;
	for (size_t i = 0; i < consumerFutureResults.size(); i++) {
		parallelTotal += consumerFutureResults[i].get();
	}
	printf("Sum after adding in parallel: %i\n", parallelTotal);

	if (serialTotal != parallelTotal) {
		fprintf(stderr, "Error: Parallel run got different result from serial run.\n");
	}

	return 0;
}
