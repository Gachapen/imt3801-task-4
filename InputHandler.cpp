#include "InputHandler.h"

#include <iostream>

void InputHandler::run(ThreadSafeQueue<int>& processQueue)
{
	std::string input;
	std::cin >> input;

	while (input != "q")
	{
		try {
			int inputNumber = std::stoi(input);
			if (inputNumber != 0) {
				printf("Input: %i\n", inputNumber);
				processQueue.push(inputNumber);
			}

		} catch (std::invalid_argument& exception) {
			printf("Input not a number.\n");
		} catch (std::out_of_range& exception) {
			printf("Input too large.\n");
		}

		std::cin >> input;
	}

	processQueue.push(0);
}
