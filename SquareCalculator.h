#ifndef SQUARECALCULATOR_H_
#define SQUARECALCULATOR_H_

#include "ThreadSafeQueue.h"

class SquareCalculator {
public:
	void run(ThreadSafeQueue<int>& outputQueue);
	ThreadSafeQueue<int>& getQueue();

private:
	ThreadSafeQueue<int> inputQueue;
};

#endif
