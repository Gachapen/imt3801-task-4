#ifndef OUTPUTPRINTER_H_
#define OUTPUTPRINTER_H_

#include "ThreadSafeQueue.h"

class OutputPrinter {
public:
	void run();
	ThreadSafeQueue<int>& getQueue();

private:
	ThreadSafeQueue<int> inputQueue;
};

#endif
