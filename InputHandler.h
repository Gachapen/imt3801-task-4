#ifndef INPUTHANDLER_H_
#define INPUTHANDLER_H_

#include "ThreadSafeQueue.h"

class InputHandler {
public:
	void run(ThreadSafeQueue<int>& processQueue);
};

#endif
