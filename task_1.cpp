#include <cstdio>
#include <thread>

#include "InputHandler.h"
#include "SquareCalculator.h"
#include "OutputPrinter.h"

int main(int argc, char* argv[])
{
	InputHandler inputHandler;
	SquareCalculator squareCalculator;
	OutputPrinter outputPrinter;

	std::thread inputThread(&InputHandler::run, &inputHandler, std::ref(squareCalculator.getQueue()));
	std::thread squareThread(&SquareCalculator::run, &squareCalculator, std::ref(outputPrinter.getQueue()));
	std::thread outputThread(&OutputPrinter::run, &outputPrinter);

	inputThread.join();
	squareThread.join();
	outputThread.join();

	return 0;
}
